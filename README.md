# MongoTEL API Client Library

> Interface with the MongoTEL API Gateway

## API

### Summary

## Development

Install application dependencies:

`npm install`

### Local Development

Develop the application locally with Storybook:

`npm run storybook`

## Quality Assurance

### Testing

Run integration and unit test:

`npm test`

Start test in watch mode:

`npm run test:watch`

### Code Coverage `TODO`

Generate a coverage report:

`npm run coverage`

### Code Formatting

Run TSLint and Prettier to analyze source code:

`npm run format`

## License

This project is licensed under the `GPLv2` License - see the [LICENSE.md](LICENSE.md) file for details