import React from "react";
export interface ExampleComponentProps {
    theme: "primary" | "secondary";
}
declare const ExampleComponent: React.FC<ExampleComponentProps>;
export default ExampleComponent;
