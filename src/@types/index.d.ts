declare enum HttpStatus {
  OK = 200,
  CREATED = 201,
  ACCEPTED = 202,
  BAD_REQUEST = 400,
  NOT_FOUND = 404,
  INTERNAL_ERROR = 500,
  SERVICE_UNAVAILABLE = 503,
}

declare interface RestErrorType {
  /** http response status error code */
  code: HttpStatus | number | string;
  /** breif human-readable specific to this error */
  title: string;
  /** a longer more detailed explanation of the issue */
  message?: string;
}

declare interface DataEnvelopePayload<T = any> {
  ok: boolean;
  summary?: string;
  count?: number;
  data?: T | T[];
  errors?: RestErrorType[];
  paging?: {
    /** index of the first item fetched */
    index?: number;

    /** total number of resources availble */
    total?: number;

    /** a url for the previous page, if any */
    previous?: string;

    /** a url for the next page, if any */
    next?: string;

    /** a url for requesting the current resource */
    self?: string;
  };
}
