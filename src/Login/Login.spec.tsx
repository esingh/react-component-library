import React from "react";
import { render } from "@testing-library/react";

import ExampleComponent, { ExampleComponentProps } from "./Login";

describe("Test Component", () => {
  let props: ExampleComponentProps;

  beforeEach(() => {
    props = {
      theme: "primary"
    };
  });

  const renderComponent = () => render(<ExampleComponent {...props} />);

  it("should have primary className with default props", () => {
    const { getByTestId } = renderComponent();

    const ExampleComponent = getByTestId("test-component");

    expect(ExampleComponent).toHaveClass("test-component-primary");
  });

  it("should have secondary className with theme set as secondary", () => {
    props.theme = "secondary";
    const { getByTestId } = renderComponent();

    const ExampleComponent = getByTestId("test-component");

    expect(ExampleComponent).toHaveClass("test-component-secondary");
  });
});