import React from "react";
import Login from './Login';
import { ApiStateProvider } from '../Provider/Provider';

export default {
  title: "Example Login"
};

export const ExampleLogin = () => <ApiStateProvider><Login/></ApiStateProvider>;
