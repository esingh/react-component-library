import React, { useContext } from "react";
import { ApiStateContext } from "../Provider/Provider";

const Login = () => {
  const { isLoggedIn, auth } = useContext(ApiStateContext);
  
  const handleSubmit = event => {
    event.preventDefault();
    const form = new FormData(event.target);
    auth.login(
      form.get('email') as string,
      form.get('password') as string
    )
  }
  return(
    <div style={{ display: 'flex', flexDirection: 'column',  justifyContent: 'center', alignItems: 'center' }}>
      <h1>MongoTEL Login</h1>
      <div>
        {isLoggedIn ? <span>User Logged In</span> : <span>Awaiting auth...</span>}
      </div>
      <form onSubmit={handleSubmit} style={{ width: '50%' }}>
        <div style={{ display: 'flex', margin: '2em 0', justifyContent: 'space-between' }}>
          <label htmlFor="email">Email</label>
          <input id="email" name="email" type="email" placeholder="Email" required/>
        </div>
        <div style={{ display: 'flex', margin: '2em 0', justifyContent: 'space-between' }}>
          <label htmlFor="password">Password</label>
          <input id="password" name="password" type="password" placeholder="Password" required/>
        </div>
        <div style={{ display: 'flex', justifyContent: 'center' }}>
          <button type="submit" disabled={isLoggedIn}>
            {isLoggedIn ? 'User Already Logged In' : 'Login'}
          </button>
        </div>
      </form>
      <br/>
      <button onClick={auth.logout} disabled={!isLoggedIn}>
        Logout
      </button>
    </div>
  )
};

export default Login;