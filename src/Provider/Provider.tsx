
import React from 'react';
import { client, auth } from '../api';

export interface ApiState {
    isLoggedIn: boolean;
    user?: {
        isActive: boolean;
        userId: number;
        username: string;
        email: string;
        name: [ string, string, string ];
        phone: string;
        maxEntityType: string;
        entityRelations: any[];
    };
    auth: {
        login?: (e: string, p: string) => void
        logout?: () => void;
    }
}

export const defaultState: ApiState = {
    isLoggedIn: false,
    user: undefined,
    auth: {
        login: undefined,
        logout: undefined
    }
};

export const ApiStateContext = React.createContext<ApiState>(defaultState);

export class ApiStateProvider extends React.Component {
    constructor(props: any) {
        super(props);
        this.state = {
            ...defaultState,
            isLoggedIn: client.isLoggedIn,
            auth: {
                login: async (email: string, password: string) => {
                    if (await auth.login(email, password)) {
                        this.setState({ isLoggedIn: true }, async () => {
                            const { activated, id, username, email, phone, max_entity_type, user_in_entity, first_name, middle_name, last_name } = await auth.me();
                            this.setState({ user: {
                                isActive: activated,
                                userId: id,
                                username,
                                email,
                                name: [ first_name, middle_name, last_name ],
                                phone,
                                maxEntityType: max_entity_type,
                                entityRelations: user_in_entity.map(({ entity_activated, entity_id, entity_name, entity_type, entity_uri, is_activated, is_active, is_super, pk }) => ({
                                    pk,
                                    entity: {
                                        id: entity_id,
                                        activated: entity_activated,
                                        name: entity_name,
                                        type: entity_type,
                                        uri: entity_uri,
                                    },
                                    isActivated: is_activated,
                                    isActive: is_active,
                                    isSuper: is_super
                                }))
                            } });
                        })
                    }

                },
                logout: () => {
                    this.setState({ isLoggedIn: client.logout(), user: undefined })
                },
            }
        };
    }
    render() {
        return (
            <ApiStateContext.Provider value={this.state as any}>
                {this.props.children}
            </ApiStateContext.Provider>
        );
    }
}