import client from './client';
import { reduceApiResults } from './utils';

/** Login user with MongoTEL credentials */
async function login(email: string, password: string): Promise<boolean> {
  const loginRequest = client.query('auth', 'POST');
  const {
    ok,
    data: { token },
  } = reduceApiResults(
    await loginRequest('login', { data: { email, password } }),
  );
  if (ok) {
    client.accessToken = token;
  }
  return ok;
}

/** Access my user's profile */
async function me(): Promise<any> {
  const authRequest = client.query('auth', 'GET');
  const { ok, data: userProfile } = reduceApiResults(await authRequest('me'));
  return userProfile;
}

/** Access my user's permissions */
async function permissions(): Promise<any> {
  const authRequest = client.query('auth', 'GET');
  const { ok, data: permissionAcl } = reduceApiResults(
    await authRequest('me/permissions'),
  );
  return permissionAcl;
}

function logout(): boolean {
  client.accessToken = null;
  return !client.isLoggedIn;
}

export default { login, me, permissions, logout };
