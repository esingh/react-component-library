import axios, {
  AxiosInstance,
  Method,
  AxiosRequestConfig,
  AxiosResponse,
} from 'axios';

type ApiEndpoint = 'auth' | 'mongopbx';

/** Provides an injectable interface to the webservice API client */
export class WebServiceClient {
  private static readonly localStorageKey = 'accessToken';
  public static readonly apiVersion = 'v1';
  private static restApi: AxiosInstance = axios.create({
    baseURL:
      process.env.NODE_ENV === 'development'
        ? 'http://localhost:8000'
        : 'https://api.mongotel.com',
    withCredentials: false,
  });
  public readonly endpoints: { [resource: string]: string } = {
    auth: '/auth',
    mongopbx: '/mongopbx',
  };

  /** Request resource from Commission API endpoint */
  query(
    resource: ApiEndpoint,
    action: Method,
    hasFiles = false,
  ): (
    accessor?: string,
    options?: AxiosRequestConfig,
  ) => Promise<AxiosResponse<any>> {
    if (resource in this.endpoints) {
      return (
        accessor = '',
        options?: AxiosRequestConfig,
      ): Promise<AxiosResponse<any>> => {
        return WebServiceClient.restApi.request({
          url: this.endpoints[resource] + (accessor ? '/' + accessor : ''),
          method: action,
          headers: {
            authorization: this.accessToken
              ? `Bearer ${this.accessToken}`
              : undefined,
            'Content-Type': hasFiles
              ? 'multipart/form-data'
              : 'application/json',
          },
          ...options,
        });
      };
    } else {
      throw new Error(`"${resource}" is not a MongoTEL API endpoint`);
    }
  }

  // ? Getter & Setter methods for interfacing JWT from localStorage

  get accessToken(): string {
    return localStorage.getItem(WebServiceClient.localStorageKey) || '';
  }

  set accessToken(token: string) {
    localStorage.setItem(WebServiceClient.localStorageKey, token);
  }

  // ? Getter for reading user auth state
  get isLoggedIn(): boolean {
    return localStorage.getItem(WebServiceClient.localStorageKey) !== null;
  }

  logout(): boolean {
    localStorage.removeItem(WebServiceClient.localStorageKey);
    return this.isLoggedIn;
  }
}

// ? Singleton instance for app use
const webServiceClient = new WebServiceClient();

export default webServiceClient;
