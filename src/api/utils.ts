import { AxiosResponse } from 'axios';

export function reduceApiResults(result: AxiosResponse): DataEnvelopePayload {
  return result.data.payload as DataEnvelopePayload;
}
